-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2023 at 03:44 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_plazaa`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `streetno` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `street`, `streetno`, `zip`, `city`, `phone`) VALUES
(1, 'Ruchit', 'Soni', NULL, NULL, NULL, NULL, NULL),
(4, 'Manish', 'Kaushik', NULL, NULL, NULL, NULL, NULL),
(6, 'Ruchit', 'Soni', NULL, NULL, NULL, NULL, NULL),
(8, 'Manish', 'Kaushik', NULL, NULL, NULL, NULL, NULL),
(16, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(17, 'Manish', 'Kaushik', '', NULL, '', '', NULL),
(18, 'Manish', 'Kaushik', '', NULL, '', '', NULL),
(19, 'Manish', 'Kaushik', '', NULL, '', '', NULL),
(20, 'rajesh', 'patel', '', NULL, '', '', NULL),
(21, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(22, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(23, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(24, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(25, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(26, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(27, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(28, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(29, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(30, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(31, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(32, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(33, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(34, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(35, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(36, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(37, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(38, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(39, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(40, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(41, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(42, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(43, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(44, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(45, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(46, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(47, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(48, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(49, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(50, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(51, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(52, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(53, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(54, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(55, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(56, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(57, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(58, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(59, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(60, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(61, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(62, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(63, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(64, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(65, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(66, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(67, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(68, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(69, 'Ruchit', 'Soni', '', NULL, '', '', NULL),
(70, 'Ruchit', 'Soni', '', NULL, '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `extra_id` int(11) NOT NULL,
  `extra_name` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `isChoosable` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`extra_id`, `extra_name`, `price`, `isChoosable`) VALUES
(1, 'Tomaten', 1.00, 0),
(2, 'Käse', 1.00, 0),
(3, 'Sardellen', 1.00, 1),
(4, 'Oliven', 1.00, 1),
(5, 'Salami', 0.50, 1),
(6, 'Schinken', 0.50, 1),
(7, 'Champignons', 0.75, 1),
(8, 'Paprika', 0.50, 1),
(9, 'Mozzarella', 1.00, 1),
(10, 'Basilikum', 0.75, 1),
(11, 'Peperoniwurst', 1.00, 1),
(12, 'Zwiebeln', 0.50, 1),
(13, 'Knoblauch', 0.50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orderitem_has_extra`
--

CREATE TABLE `orderitem_has_extra` (
  `id` int(11) NOT NULL,
  `orderitems_id` int(11) DEFAULT NULL,
  `extra_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `Id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Id`, `timestamp`, `customer_id`) VALUES
(1, '2023-07-18 10:22:28', 8),
(2, '2023-07-18 15:28:21', 16),
(3, '2023-07-18 15:47:05', 17),
(4, '2023-07-18 17:11:30', 18),
(5, '2023-07-18 17:13:46', 19),
(6, '2023-07-18 17:59:51', 20),
(7, '2023-07-18 19:28:17', 21),
(8, '2023-07-18 20:32:09', 22),
(9, '2023-07-19 09:08:20', 23),
(10, '2023-07-19 09:19:16', 24),
(11, '2023-07-19 09:30:37', 25),
(12, '2023-07-19 09:31:26', 26),
(13, '2023-07-19 09:33:37', 27),
(14, '2023-07-19 10:24:05', 28),
(15, '2023-07-19 10:24:46', 29),
(16, '2023-07-19 10:27:45', 30),
(17, '2023-07-19 10:28:33', 31),
(18, '2023-07-19 10:35:48', 32),
(19, '2023-07-19 10:39:12', 33),
(20, '2023-07-19 10:39:29', 34),
(21, '2023-07-19 10:39:52', 35),
(22, '2023-07-19 10:40:21', 36),
(23, '2023-07-19 10:41:12', 37),
(24, '2023-07-19 10:43:41', 38),
(25, '2023-07-19 10:43:57', 39),
(26, '2023-07-19 10:45:23', 40),
(27, '2023-07-19 10:45:49', 41),
(28, '2023-07-19 10:46:38', 42),
(29, '2023-07-19 10:50:59', 43),
(30, '2023-07-19 10:54:14', 44),
(31, '2023-07-19 10:54:51', 45),
(32, '2023-07-19 11:29:11', 46),
(33, '2023-07-19 11:37:08', 47),
(34, '2023-07-19 11:39:12', 48),
(35, '2023-07-19 11:41:27', 49),
(36, '2023-07-19 11:42:01', 50),
(37, '2023-07-19 11:53:13', 51),
(38, '2023-07-19 11:54:31', 52),
(39, '2023-07-19 11:55:47', 53),
(40, '2023-07-19 12:03:00', 54),
(41, '2023-07-19 12:05:14', 55),
(42, '2023-07-19 12:05:37', 56),
(43, '2023-07-19 12:05:53', 57),
(44, '2023-07-19 12:07:20', 58),
(45, '2023-07-19 12:07:27', 59),
(46, '2023-07-19 12:08:12', 60),
(47, '2023-07-19 12:14:31', 61),
(48, '2023-07-19 12:15:06', 62),
(49, '2023-07-19 12:17:23', 63),
(50, '2023-07-19 12:19:30', 64),
(51, '2023-07-19 12:19:37', 65),
(52, '2023-07-19 12:20:51', 66),
(53, '2023-07-19 12:21:17', 67),
(54, '2023-07-19 12:21:36', 68),
(55, '2023-07-19 12:34:07', 69),
(56, '2023-07-19 12:34:24', 70);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `pizza_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

CREATE TABLE `pizzas` (
  `pizza_id` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`pizza_id`, `price`, `description`, `name`) VALUES
(4, 4.00, 'Eine leckere Käsepizza.', 'Magherita'),
(5, 5.50, 'Ein Mix aus Sardellen und Oliven bieten den ultimativen Flair.', 'Napoli'),
(6, 4.70, 'Der Klassiker unter den Pizzen.', 'Salami'),
(7, 4.70, 'Der im Volksmund auch als Schinkenpizza bekannte Klassiker.', 'Prosciutto'),
(8, 4.70, 'Die beste Wahl für alle Pilzliebhaber.', 'Funghi'),
(9, 5.50, 'Die beste Kombination zwischen deftig und frisch!', 'Salami-Paprika'),
(10, 5.50, '100% vegetarisch - 100% lecker!', 'Tricolore'),
(11, 5.50, 'Teuflisch scharf!', 'Diavolo'),
(12, 5.50, 'Schinken und Champignons, köstlich im Steinofen zubereitet.', 'Roma');

-- --------------------------------------------------------

--
-- Table structure for table `pizza_has_extra`
--

CREATE TABLE `pizza_has_extra` (
  `id` int(11) NOT NULL,
  `Pizzas_id` int(11) DEFAULT NULL,
  `extra_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pizza_has_extra`
--

INSERT INTO `pizza_has_extra` (`id`, `Pizzas_id`, `extra_id`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 5, 1),
(4, 5, 2),
(5, 5, 3),
(6, 5, 4),
(7, 6, 1),
(8, 6, 2),
(9, 6, 5),
(10, 7, 1),
(11, 7, 2),
(12, 7, 6),
(13, 8, 1),
(14, 8, 2),
(15, 8, 7),
(16, 9, 1),
(17, 9, 2),
(18, 9, 5),
(19, 9, 8),
(20, 10, 1),
(21, 10, 2),
(22, 10, 9),
(23, 10, 10),
(24, 11, 1),
(25, 11, 2),
(26, 11, 11),
(27, 11, 12),
(28, 11, 13),
(29, 12, 1),
(30, 12, 2),
(31, 12, 11),
(32, 12, 12),
(33, 12, 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`extra_id`);

--
-- Indexes for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderitems_id` (`orderitems_id`),
  ADD KEY `extra_id` (`extra_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Id`) USING BTREE,
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `pizza_id` (`pizza_id`);

--
-- Indexes for table `pizzas`
--
ALTER TABLE `pizzas`
  ADD PRIMARY KEY (`pizza_id`);

--
-- Indexes for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Pizzas_id` (`Pizzas_id`),
  ADD KEY `extra_id` (`extra_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `extra_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `pizzas`
--
ALTER TABLE `pizzas`
  MODIFY `pizza_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderitem_has_extra`
--
ALTER TABLE `orderitem_has_extra`
  ADD CONSTRAINT `orderitem_has_extra_ibfk_1` FOREIGN KEY (`orderitems_id`) REFERENCES `order_items` (`id`),
  ADD CONSTRAINT `orderitem_has_extra_ibfk_2` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`extra_id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`pizza_id`) REFERENCES `pizzas` (`pizza_id`);

--
-- Constraints for table `pizza_has_extra`
--
ALTER TABLE `pizza_has_extra`
  ADD CONSTRAINT `pizza_has_extra_ibfk_1` FOREIGN KEY (`Pizzas_id`) REFERENCES `pizzas` (`pizza_id`),
  ADD CONSTRAINT `pizza_has_extra_ibfk_2` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`extra_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
