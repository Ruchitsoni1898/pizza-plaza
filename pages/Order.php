<?php
include("Article.php");

// Check if the form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Retrieve the customer's first and last name from the form submission
    $firstName = $_POST['first_name'];
    $lastName = $_POST['last_name'];

    // Retrieve the address fields if the delivery option is selected
    $address = $city = $zipcode = '';
    if (isset($_POST['street']) && isset($_POST['city']) && isset($_POST['zip'])) {
        $address = $_POST['street'];
        $city = $_POST['city'];
        $zipcode = $_POST['zip'];
    }

    // Validate the input data (e.g., check for empty fields, perform format validation)

    // Connect to the database
    $dbHost = '127.0.0.1';
    $dbName = 'pizza_plazaa';
    $dbUser = 'root';
    $dbPass = '';

    try {
        $conn = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Start a database transaction
        $conn->beginTransaction();

        // Insert the customer into the customers table
        $customerStmt = $conn->prepare("INSERT INTO customers (first_name, last_name, street, zip, city) VALUES (:first_name, :last_name, :street, :zip, :city)");
        $customerStmt->bindParam(':first_name', $firstName);
        $customerStmt->bindParam(':last_name', $lastName);
        $customerStmt->bindParam(':street', $address);
        $customerStmt->bindParam(':zip', $zipcode);
        $customerStmt->bindParam(':city', $city);
        $customerStmt->execute();

        // Retrieve the auto-generated customer ID
        $customerId = $conn->lastInsertId();

        // Insert the order into the orders table
        $orderStmt = $conn->prepare("INSERT INTO orders (customer_id, timestamp) VALUES (:customer_id, NOW())");
        $orderStmt->bindParam(':customer_id', $customerId);
        $orderStmt->execute();

        // Retrieve the auto-generated order ID
        $orderId = $conn->lastInsertId();

        // Commit the transaction
        $conn->commit();

        // Retrieve the pizza details
        $pizzaQuery = $conn->prepare("SELECT * FROM pizzas");
        $pizzaQuery->execute();
        $pizzas = $pizzaQuery->fetchAll(PDO::FETCH_ASSOC);

        if (count($pizzas) > 0) {
            $pizza = $pizzas[0];
            $pizzaId = $pizza['pizza_id'];
            $pizzaName = $pizza['name'];
            $pizzaPrice = $pizza['price'];

            // Retrieve the extras and quantity from the form submission
            $selectedPizzaId = $pizza['pizza_id'];
            $extras = $_POST['extras_'.$selectedPizzaId] ?? '';
            $quantity = $_POST['quantity_'.$selectedPizzaId] ?? '';

            // Redirect to the confirmation page and pass the order ID, pizza ID, pizza name, pizza price, extras, and quantity
            $queryString = http_build_query([
                'order_id' => $orderId,
                'pizza_id' => $pizzaId,
                'name' => $pizzaName,
                'price' => $pizzaPrice,
                'extras' => $extras,
                'quantity' => $quantity,
            ]);

            header('Location: confirmation.php?' . $queryString);
            exit();
        } else {
            echo "No pizza details found.";
        }
    } catch (PDOException $e) {
        // Rollback the transaction on error
        $conn->rollBack();

        // Handle database connection or query errors
        // Display an error message or redirect to an error page
        echo "Error: " . $e->getMessage();
    } finally {
        // Close the database connection
        $conn = null;
    }
}
?>
