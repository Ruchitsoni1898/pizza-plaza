<!DOCTYPE html>
<html>
<head>
    <title>Backend - Orders</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }

        .delete-form {
            display: inline-block;
        }

        .delete-form button {
            background-color: #f44336;
            color: white;
            padding: 8px 12px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .delete-form button:hover {
            background-color: #d32f2f;
        }
    </style>
</head>
<body>
    <h1>Backend - Orders</h1>
    <?php
    // Check if the password form is submitted
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $password = $_POST['password'];

        // Validate the password
        if ($password === 'Ruchit') {
            // Password is correct, display the orders
            displayOrders();
        } else {
            // Password is incorrect, display an error message
            echo "<p>Invalid password!</p>";
            displayPasswordForm();
        }
    } else {
        // Display the password form
        displayPasswordForm();
    }

    // Function to display the password form
    function displayPasswordForm()
    {
        ?>
        <form method="post" action="">
            <label for="password">Enter Password:</label>
            <input type="password" name="password" id="password" required>
            <button type="submit">Submit</button>
        </form>
        <?php
    }

    // Function to display the orders
    function displayOrders()
    {
        // Connect to the database and fetch the orders
        // Replace the database connection details and query with your own
        $dbHost = '127.0.0.1';
        $dbName = 'pizza_plazaa';
        $dbUser = 'root';
        $dbPass = '';

        try {
            $conn = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Fetch all orders with customer data, total amount, and items/extras
            $ordersQuery = "SELECT o.id AS order_id, o.timestamp, c.first_name, c.last_name, c.street, c.streetno, c.zip, c.city, c.phone
            FROM orders o
            JOIN customers c ON o.customer_id = c.id";

            $stmt = $conn->prepare($ordersQuery);
            $stmt->execute();
            $orders = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Fetch all pizzas with extras
            $pizzasQuery = "SELECT p.pizza_id, p.price, p.description, p.name, e.extra_id, e.extra_name, e.price AS extra_price, e.isChoosable
            FROM pizzas p
            JOIN pizza_has_extra pe ON p.pizza_id = pe.Pizzas_id
            JOIN extras e ON pe.extra_id = e.extra_id";


            $stmt = $conn->prepare($pizzasQuery);
            $stmt->execute();
            $pizzas = $stmt->fetchAll(PDO::FETCH_ASSOC);


            // Display the orders in a table
            if (!empty($orders)) {
                ?>
                <table>
                    <tr>
                        <th>Order ID</th>
                        <th>Customer Name</th>
                        <th>Order Date</th>
                        <th>Total Amount</th>
                        <th>Items</th>
                        <th>Extras</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($orders as $order) : ?>
                        <tr>
                            <td><?php echo $order['order_id']; ?></td>
                            <td><?php echo $order['first_name'] . ' ' . $order['last_name']; ?></td>
                            <td><?php echo $order['timestamp']; ?></td>
                            <td><?php echo $order['total_amount']; ?></td>
                            <td><?php echo $order['all_items']; ?></td>
                            <td><?php echo $order['all_extras']; ?></td>
                            <td>
                                <form class="delete-form" method="post" action="delete_order.php">
                                    <input type="hidden" name="order_id" value="<?php echo $order['order_id']; ?>">
                                    <button type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <?php
            } else {
                echo "<p>No orders found.</p>";
            }
        } catch (PDOException $e) {
            // Handle database connection or query errors
            // Display an error message or redirect to an error page
            echo "Error: " . $e->getMessage();
        } finally {
            // Close the database connection
            $conn = null;
        }
    }
    ?>
</body>
</html>