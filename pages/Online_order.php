<?php
use App\Kernel;
// Include the Article.php component
require_once  "Article.php";
// Create an instance of the Article component
$article = new Article();

// Get all pizzas from the database
$pizzas = $article->getAllPizzas();

$extras = $article->getAllExtras();
?>
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th, td {
        padding: 10px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: #f2f2f2;
    }

    input[type="number"] {
        width: 50px;
    }

    button {
        background-color: #4CAF50;
        color: white;
        padding: 8px 12px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    button:hover {
        background-color: #45a049;
    }

    .success-message {
        color: green;
        font-weight: bold;
        margin-top: 10px;
    }
</style>


<table>
    <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Quantity</th>
        <th>Extras</th>
        <th>Action</th>
    </tr>
    <?php foreach ($pizzas as $pizza) : ?>
    <tr>
        <td><?php echo $pizza['name']; ?></td>
        <td><?php echo $pizza['price']; ?></td>
        <td><?php echo $pizza['description']; ?></td>
        <td><input type="number" name="quantity_<?php echo $pizza['pizza_id']; ?>"></td>
        <td>
    <?php if (!empty($extras)) : ?>
        <select name="extras<?php echo $pizza['pizza_id']; ?>">
            <option value="">-- Select Ingredients --</option>
            <?php foreach ($extras as $extra) : ?>
                <?php
                $selectedExtra = $_GET['extra_name'.$pizza['pizza_id']] ?? '';
                $selected = ($extra['extra_name'] === $selectedExtra) ? 'selected' : '';
                ?>
                <option value="<?php echo $extra['extra_name']; ?>" <?php echo $selected; ?>><?php echo htmlspecialchars($extra['extra_name']); ?></option>
            <?php endforeach; ?>
        </select>
    <?php endif; ?>
</td>


</td>
        <td>
            <button onclick="addToCart(<?php echo $pizza['pizza_id']; ?>, '<?php echo $pizza['name']; ?>', '<?php echo $pizza['price']; ?>')">Add to Cart</button>       </td>
    </tr>
    <?php endforeach; ?>
</table>

<div id="message" class="success-message"></div>

<script>
function addToCart(pizzaId, pizzaName, pizzaPrice) {
    // Get the quantity value for the selected pizza
    const quantityInput = document.querySelector(`input[name="quantity_${pizzaId}"]`);
    const quantity = quantityInput.value;

    // Get the selected extras (if applicable)
    const extrasSelect = document.querySelector(`select[name="extras${pizzaId}"]`);
    const extras = extrasSelect.value;

    // Encode pizza details for URL
    const encodedPizzaName = encodeURIComponent(pizzaName);
    const encodedPizzaPrice = encodeURIComponent(pizzaPrice);

    // Construct the URL with pizza details and success message as query parameters
    const url = `Checkout.php?id=${pizzaId}&quantity=${quantity}&extras=${extras}&name=${encodedPizzaName}&price=${encodedPizzaPrice}&success=true`;

    // Display the success message
    const messageElement = document.getElementById('message');
    messageElement.innerText = 'Product added successfully to the cart! process to checkout...';
    window.location.href = url;
}

function buyNow(pizzaId, pizzaName, pizzaPrice) {
    // Redirect to the login page
    window.location.href = "Backend.php";
}
</script>





