<?php
// confirmation.php

// Retrieve the order ID from the URL query parameters
$orderId = $_GET['order_id'] ?? '';
$pizzaId = $_GET['pizza_id'] ?? '';
$pizzaName = $_GET['name'] ?? '';
$pizzaPrice = $_GET['price'] ?? '';
$quantity = $_GET['quantity'] ?? '';
$extras = $_GET['extras'] ?? '';

// Connect to the database
$dbHost = '127.0.0.1';
$dbName = 'pizza_plazaa';
$dbUser = 'root';
$dbPass = '';

try {
    $conn = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Fetch the order details from the database
    $stmt = $conn->prepare("SELECT * FROM orders WHERE id = :id");
    $stmt->bindParam(':id', $orderId);
    $stmt->execute();

    // Check if the order exists
    if ($stmt->rowCount() > 0) {
        $order = $stmt->fetch(PDO::FETCH_ASSOC);
        $customerId = $order['customer_id'];

        // Fetch the customer details from the database
        $customerStmt = $conn->prepare("SELECT * FROM customers WHERE id = :id");
        $customerStmt->bindParam(':id', $customerId);
        $customerStmt->execute();
        $customer = $customerStmt->fetch(PDO::FETCH_ASSOC);

        // Display the order confirmation details
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <title>Order Confirmation</title>
            <style>
                /* Add your custom CSS styles here */
                body {
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 20px;
                }

                h1 {
                    font-size: 24px;
                    margin-bottom: 20px;
                }

                .order-details {
                    background-color: #f2f2f2;
                    padding: 20px;
                    margin-bottom: 20px;
                }

                .order-details h3 {
                    margin-top: 0;
                }

                .order-details p {
                    margin: 5px 0;
                }
            </style>
        </head>
        <body>
            <h1>Order Confirmation</h1>

            <div class="order-details">
                <h3>Order Details</h3>
                <p>Order ID: <?php echo $order['Id']; ?></p>
                <p>Customer Name: <?php echo $customer['first_name'] . ' ' . $customer['last_name']; ?></p>
                <p>Order Timestamp: <?php echo $order['timestamp']; ?></p>
            </div>

            <div class="pizza-details">
                <h3>Pizza Details</h3>
                <p>ID: <?php echo $pizzaId; ?></p>
                <p>Name: <?php echo $pizzaName; ?></p>
                <p>Price: <?php echo $pizzaPrice; ?></p>
                <p>Quantity: <?php echo $quantity; ?></p>
                <p>Extras: <?php echo $extras; ?></p>
            </div>
        </body>
        </html>
        <?php
    } else {
        echo "Order not found.";
    }
} catch (PDOException $e) {
    // Handle database connection or query errors
    // Display an error message or redirect to an error page
    echo "Error: " . $e->getMessage();
} finally {
    // Close the database connection
    if (isset($conn)) {
        $conn = null;
    }
}
?>
