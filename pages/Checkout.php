<?php

include "Article.php";
// Retrieve the pizza details from the URL query parameters
$pizzaId = $_GET['id'] ?? '';
$quantity = $_GET['quantity'] ?? '';
$selectedExtra = $_GET['extras'] ?? '';
$pizzaName = $_GET['name'] ?? '';
$pizzaPrice = $_GET['price'] ?? '';

// Check if items have already been added to the order
$orderItemsCount = 0;
$orderTotalPrice = 0;

if (!empty($pizzaId) && !empty($quantity) && !empty($pizzaPrice)) {
    $orderItemsCount = 1;
    $orderTotalPrice = $quantity * $pizzaPrice;
}

// Check if the order value qualifies for delivery
$deliveryOption = false;
$deliveryFee = 0;

if ($orderTotalPrice >= 10.00) {
    $deliveryOption = true;
    $deliveryFee = ($orderTotalPrice > 25.00) ? 0 : 1.50;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Checkout</title>
    <style>
        /* Add your custom CSS styles here */

        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        .checkout-container {
            max-width: 500px;
            margin: 0 auto;
            padding: 20px;
            background-color: #f2f2f2;
            border-radius: 5px;
        }

        .checkout-container h2 {
            margin-top: 0;
            margin-bottom: 20px;
        }

        .checkout-form {
            display: grid;
            gap: 10px;
        }

        .checkout-form label {
            font-weight: bold;
        }

        .checkout-form input[type="text"],
        .checkout-form input[type="email"] {
            width: 100%;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        .checkout-form button {
            background-color: #4CAF50;
            color: white;
            padding: 8px 12px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .checkout-form button:hover {
            background-color: #45a049;
        }

        .order-summary {
            background-color: #f2f2f2;
            padding: 10px;
            margin-bottom: 20px;
            cursor: pointer;
        }

        .order-summary p {
            margin: 5px 0;
        }

        .pizza-details {
            background-color: #f2f2f2;
            padding: 20px;
            margin-bottom: 20px;
        }

        .pizza-details h3 {
            margin-top: 0;
            margin-bottom: 10px;
        }

        .pizza-details p {
            margin: 5px 0;
        }
    </style>
</head>
<body>
<div class="checkout-container">
    <h1>Checkout</h1>

    <div class="order-summary" onclick="goToCheckout()">
        <?php if ($orderItemsCount > 0) : ?>
            <p>Number of Items: <?php echo $orderItemsCount; ?></p>
            <p>Total Price: €<?php echo $orderTotalPrice; ?></p>
        <?php else : ?>
            <p>No items in the order</p>
        <?php endif; ?>
    </div>

    <div class="pizza-details">
        <h3>Pizza Details</h3>
        <p>ID: <?php echo $pizzaId; ?></p>
        <p>Name: <?php echo $pizzaName; ?></p>
        <p>Price: <?php echo $pizzaPrice; ?></p>
        <p>Quantity: <?php echo $quantity; ?></p>
        <p>Extras: <?php echo $selectedExtra; ?></p>
    </div>

    <form class="checkout-form" action="Backend.php" method="POST">
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name" required>

        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name" required>

        <?php if ($deliveryOption) : ?>
            <label for="address">Address:</label>
            <input type="text" name="address" id="address" required>

            <label for="city">City:</label>
            <input type="text" name="city" id="city" required>

            <label for="zipcode">Zip Code:</label>
            <input type="text" name="zipcode" id="zipcode" required>
        <?php endif; ?>

        <button type="submit">Place Order</button>
    </form>

    <?php if ($deliveryOption) : ?>
        <p>Delivery Fee: €<?php echo $deliveryFee; ?></p>
    <?php endif; ?>

</div>
</body>
</html>



