<?php
// Include the functions.php file
require_once "Function.php";
require_once "Article.php";

// Fetch the list of available pizzas from your database or data source
$pizzas = [
    [
        'name' => 'Magherita',
        'description' => 'Eine leckere Käsepizza.',
        'price' => 4.00,
    ],
    [
        'name' => 'Napoli',
        'description' => 'Ein Mix aus Sardellen und Oliven bieten den ultima...',
        'price' =>5.50 ,
    ],
    [
        'name' => 'Salami',
        'description' => 'Der Klassiker unter den Pizzen.',
        'price' =>4.70 ,
    ],
    [
        'name' => 'Prosciutto',
        'description' => 'Der im Volksmund auch als Schinkenpizza bekannte Klassiker.',
        'price' => 4.70,
    ],
    [
        'name' => 'Funghi',
        'description' => 'Die beste Wahl für alle Pilzliebhaber.',
        'price' => 4.70,
    ],
    [
        'name' => 'Salami-Paprika',
        'description' => 'Die beste Kombination zwischen deftig und frisch!',
        'price' => 5.50,
    ],
    [
        'name' => 'Tricolore',
        'description' => '100% vegetarisch - 100% lecker!',
        'price' => 5.50,
    ],
    [
        'name' => 'Diavolo',
        'description' => 'Teuflisch scharf!',
        'price' => 5.50,
    ],
    [
        'name' => 'Roma',
        'description' => 'Schinken und Champignons, köstlich im Steinofen zubereitet.',
        'price' => 5.50,
    ],

    
    


];

// Get the pizza of the day
$pizzaOfTheDay = getRandomPizzaOfTheDay($pizzas);
?>

<!-- main.php -->

<!DOCTYPE html>
<html>
<head>
    <title>Pizza Plaza - Home</title>

    <!-- Include other CSS and JavaScript files -->

</head>
<body>
    <style>
        #cookie-bar {
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  background-color: #f0f0f0;
  padding: 10px;
  z-index: 9999;
}

.cookie-content {
  max-width: 600px;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.cookie-content p {
  margin: 0;
}

#cookie-accept {
  background-color: #4caf50;
  color: white;
  border: none;
  padding: 10px 20px;
  cursor: pointer;
}
</style>

    <p class="lead">Herzlich Willkommen auf der Webseite von Pizza Plaza!</p>
    <p>Wir sind ein fiktives Restaurant in Gießen/Langgöns. Auf unserer Webseite erfahren Sie nähere Informationen über uns, Wege, uns zu kontaktieren und erhalten bald die Möglichkeit, Bestellungen online durchzuführen.</p>
    <p>Ihr Restaurant Pizza Plaza</p>

    <!-- Display the pizza of the day with a discount on the start page -->
    <div>
        <h2>Pizza of the Day: <?php echo $pizzaOfTheDay['name']; ?></h2>
        <p>Description: <?php echo $pizzaOfTheDay['description']; ?></p>
        <p>Price: €<?php echo number_format($pizzaOfTheDay['price'] * 0.67, 2); ?></p>
    </div>

    <!-- Add the cookie banner HTML code -->
    <div id="cookie-bar">
  <div class="cookie-content">
    <p>This website uses cookies to improve your experience. By continuing to browse, you consent to the use of cookies.</p>
    <button id="cookie-accept">Accept</button>
  </div>
</div>
<script>
    // Check if the user has already confirmed the cookie notice
if (!localStorage.getItem('cookieAccepted')) {
  // Show the cookie bar if not confirmed
  document.getElementById('cookie-bar').style.display = 'block';
}

// Handle click event on the accept button
document.getElementById('cookie-accept').addEventListener('click', function() {
  // Set the confirmation in LocalStorage
  localStorage.setItem('cookieAccepted', 'true');

  // Hide the cookie bar
  document.getElementById('cookie-bar').style.display = 'none';
});
</script>
</body>
</html>
