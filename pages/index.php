<?php
$defaultLanguage = 'english';

// Check if the language cookie is already set
if (isset($_COOKIE['lang'])) {
    $currentLanguage = $_COOKIE['lang'];
} else {
    // Set the language cookie with a one-year expiration time
    $currentLanguage = $_GET['lang'] ?? $defaultLanguage;
    setcookie('lang', $currentLanguage, time() + (365 * 24 * 60 * 60), '/');
}

// Update the $currentLanguage variable using the cookie value
$currentLanguage = $_COOKIE['lang'] ?? $currentLanguage;

// Load the corresponding configuration file based on the current language
$configFile = ($currentLanguage === 'german') ? "pages/config_german.json" : "pages/config_english.json";

// Read the contents of the configuration file
$config = json_decode(file_get_contents($configFile), true);

// Function to translate a given key in the configuration
function translate($key)
{
    global $config;
    global $currentLanguage;

    // Check if the translation exists for the given key and current language
    if (isset($config[$key]) && isset($config[$key][$currentLanguage])) {
        return $config[$key][$currentLanguage];
    }

    // If the translation is not found, return the key itself
    return $key;
}

// Get the current site from the query parameter or set a default site
$currentSite = $_GET['site'] ?? 'main';

// Function to generate the language switcher links
function generateLanguageLinks($currentLanguage, $currentSite)
{
    $languageLinks = '';
    $languages = ['german', 'english'];

    foreach ($languages as $language) {
        $link = "?site=$currentSite&lang=$language";
        $label = ucfirst($language);
        $activeClass = ($currentLanguage === $language) ? 'active' : '';
        $languageLinks .= "<a href=\"$link\" data-lang=\"$language\" class=\"language-link $activeClass\">$label</a>";
    }

    return $languageLinks;
}

// Function to generate the breadcrumb navigation
function generateBreadcrumb($currentSite)
{
    $breadcrumb = '<nav aria-label="breadcrumb"><ol class="breadcrumb">';
    $breadcrumb .= '<li class="breadcrumb-item"><a href="?site=main">' . translate('home') . '</a></li>';

    // Check if the current site is set and not equal to 'main'
    if (isset($currentSite) && $currentSite !== 'main') {
        // Split the current site on underscores to get individual parts
        $parts = explode('_', $currentSite);
        $partCount = count($parts);

        // Loop through the parts and generate the breadcrumb items
        $url = '?site=';
        $urlParams = '';
        for ($i = 0; $i < $partCount; $i++) {
            $part = $parts[$i];
            $url .= $part;
            $urlParams .= '&lang=' . $currentLanguage;

            // Determine if it is the last part
            $isLastPart = ($i === $partCount - 1);

            // Generate breadcrumb item with arrow icon
            $breadcrumb .= '<li class="breadcrumb-item">';
            if ($isLastPart) {
                $breadcrumb .= '<span>' . ucfirst($part) . '</span>';
            } else {
                $breadcrumb .= '<a href="' . $url . $urlParams . '">' . ucfirst($part) . '</a>';
            }
            $breadcrumb .= '</li>';
        }
    }

    $breadcrumb .= '</ol></nav>';
    return $breadcrumb;
}
?>

<!DOCTYPE html>
<html lang="<?php echo $currentLanguage; ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pizza Plaza</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <style>
        .breadcrumb {
            background-color: transparent;
            margin-top: 20px;
            padding: 0;
        }

        .breadcrumb-item+.breadcrumb-item::before {
            margin-right: 0.5rem;
        }

        .breadcrumb-item a {
            color: #007bff;
            text-decoration: none;
        }

        .breadcrumb-item a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="?site=main"><?php echo translate('welcome_message'); ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php if($currentSite === 'main') echo 'active'; ?>">
                <a class="nav-link" href="?site=main"><?php echo translate('welcome_message'); ?></a>
            </li>
            <li class="nav-item <?php if($currentSite === 'about') echo 'active'; ?>">
                <a class="nav-link" href="?site=about"><?php echo translate('about_us'); ?></a>
            </li>
            <li class="nav-item <?php if($currentSite === 'contact') echo 'active'; ?>">
                <a class="nav-link" href="?site=contact"><?php echo translate('contact_us'); ?></a>
            </li>
            <li class="nav-item <?php if($currentSite === 'imprint') echo 'active'; ?>">
                <a class="nav-link" href="?site=imprint"><?php echo translate('imprint'); ?></a>
            </li>
            <li class="nav-item <?php if($currentSite === 'Online_order.php') echo 'active'; ?>">
                <a class="nav-link" href="pages/Online_order.php"><?php echo translate('online_order'); ?></a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <div class="nav-link">
                    <?php echo generateLanguageLinks($currentLanguage, $currentSite); ?>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="breadcrumb-container">
        <?php echo generateBreadcrumb($currentSite); ?>
    </div>

    <?php
    if ($currentSite === 'Online_order.php') {
        include 'pages/Online_order.php';
    } else {
        include "pages/$currentSite.php";
    }
    ?>
</div>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
<script>
    // Update the language cookie and reload the page with the new language
    document.querySelectorAll('.language-link').forEach(link => {
        link.addEventListener('click', (e) => {
            e.preventDefault();
            const lang = link.getAttribute('data-lang');
            document.cookie = `lang=${lang}; expires=${new Date(new Date().getTime() + (365 * 24 * 60 * 60 * 1000)).toUTCString()}; path=/`;
            location.href = `?site=<?php echo $currentSite; ?>&lang=${lang}`;
        });
    });
</script>
</body>
</html>

